import sys
import numpy as np

# Exercice 1 : Apprentissage des variables, fonctions et conditions simples.
# Créez une fonction comparant deux ints et retournez le plus petit. Retournez en un s'il sont égaux.

def compare(a, b):
    return None

# Exercice 2 : Apprentissage des tableaux et boucles
# Créez une fonction qui prend en paramètre un tableau et retourne ce même tableau avec ses valeurs incrémentées de 1

def ajoute_un(tab):
    return None

# Exercice 3 : Apprentissage des tableaux et boucles.
# Créez une fonction qui prend en paramètre un tableau implémentant le tri à bulles

def tri_bulle(tab):
    return None

# Exercice 4 : Un peu de travail avec les palindromes

# 4.1. : Montrez ce que vous avez appris !
# Codez un algorithme reconnaissant les palindromes pour les string (C'est faisable en une ligne !) :

def palindrome_str(s):
    return None

# 4.2. : Montrez ce que vous avez appris, encore !
# Codez un algorithme reconnaissant les palindromes pour les int en utilisant une boucle while

def palindrome_int(i):
    return None

# Exercice 5 : Les tours de Hanoi (Pour les plus rapides)
# Codez l'algorithme des tours de Hanoi retournant le nombre de mouvements effectués

def hanoi(n, a, b, c):
    return None

def main():
    # Test Exercice 1
    if (compare(1,2) == 1 and compare(1,1) == 1):
        print("compare ok")
    else:
        sys.exit("error while testing compare function")

    # Test Exercice 2
    if (np.array_equal(ajoute_un([1,2,3]),[2,3,4])):
        print("ajoute_un ok")
    else:
        sys.exit("error while testing ajoute_un function")

    # Test Exercice 3
    if (np.array_equal(tri_bulle([8,4,1,2,6]), [1,2,4,6,8])):
        print("tri_bulle ok")
    else:
        sys.exit("error while testing tri_bulle function")

    # Test Exercice 4
    # Test 4.1
    if ((palindrome_str("kayak") == True) and (palindrome_str("totoro") == False)):
        print("palindrome_str ok")
    else:
        sys.exit("error while testing palindrome_str function")

    # Test 4.2
    if ((palindrome_int(15877851) == True) and (palindrome_int(42) == False)):
        print("palindrome_int ok")
    else:
        sys.exit("error while testing palindrome_int function")

    # Test Exercice 5
    if (hanoi(10, "A", "B", "C") == 1023):
        print("hanoi ok")
    else:
        sys.exit("error while testing hanoi function")

    # Exercice bonus : Affichez pi (Pensez à l'import de modules !)

main()

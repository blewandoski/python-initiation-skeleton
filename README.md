TP Python Initiation
==============

Ce court tp vous apprendra les bases de python qui seront utiles pour le tp flask qui suivra.


Informations générales
============

    Ce tp a été conçu à l'aide d'un environnement de travail linux et son bon déroulement a donc été vérifié sur cet OS uniquement. Nous vous conseillons d'utiliser une VM si vous n'avez pas de linux. Néanmoins, sachez qu'importer et exécuter un fichier python sous windows est possible en utilisant pycharm qui appartient à JetBrains (https://www.jetbrains.com/help/pycharm/creating-flask-project.html).

    
Pré-requis
============

    $ sudo apt-get install git python3 python3-pip
    $ git clone https://gitlab.com/blewandoski/python-initiation-skeleton.git
    $ cd python-initiation
    $ pip3 install numpy

Déroulement du TP
===========

Suivez les indications contenues dans le fichier main.py. A la moindre question appelez Clément, Valentin ou Baptiste.


Exécution
===========

    $ python3 main.py

Liens Utiles
===========

https://docs.python.org/fr/3

https://docs.python.org/fr/3/tutorial/modules.html (Pour l'exercice bonus)
